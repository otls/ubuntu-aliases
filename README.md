# Ubuntu Aliases (WSL)

# PHP
```bash
php-add 8.1 # Add new PHP version
php-use 8.1 # Use PHP version
php-list # List all installed PHP versions
```

# Laravel
```bash
laravel-create my-app # Create a new Laravel application & virtual host apache
```