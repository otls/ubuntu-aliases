# PHP Helpers
alias php-add="bash ~/aliases/php/php_add.sh"
alias php-use="bash ~/aliases/php/php_switch.sh"
alias php-list="sudo update-alternatives --list php"
