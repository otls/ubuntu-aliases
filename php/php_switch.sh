php_vrsion=$1

if [ -z "$php_vrsion" ]; then
    echo "Please specify a PHP version to use. Example: php-use 7.1"
    exit 1
fi

# check if the version is installed
if [ ! -f "/usr/bin/php$php_vrsion" ]; then
    echo "PHP version $php_vrsion is not installed. Please install it first by running:"
    echo "php-add $php_vrsion"
    exit 1
fi

# get current php version only 3 digits exampl: 7.1
current_php_version=$(php -v | head -n 1 | cut -d " " -f 2 | cut -d "." -f 1,2)
current_php_version_alias="php$current_php_version"

# init new php version
new_php_version_alias="php$php_vrsion"

echo "Switching PHP version from $current_php_version to $php_vrsion"

# disable current php version
echo "Disabling $current_php_version_alias"
sudo a2dismod $current_php_version_alias

# enable new php version
echo "Enabling $php_vrsion"
sudo a2enmod $new_php_version_alias

# set new php version as default
echo "Setting $php_vrsion as default"
sudo update-alternatives --set php /usr/bin/php$php_vrsion
# set phar to use new php version
echo "Setting phar to use $php_vrsion"
sudo update-alternatives --set phar /usr/bin/phar$php_vrsion

# restart apache
echo "Restarting apache"
sudo service apache2 restart

php -v
