php_version=$1
php_packages_list=("php[php_ver]" "php[php_ver]-common" "php[php_ver]-redis" "php[php_ver]-snmp" "php[php_ver]-xml" "php[php_ver]-zip" "php[php_ver]-mbstring" "libapache2-mod-php[php_ver]" "php[php_ver]-gd" "php[php_ver]-intl" "php[php_ver]-xsl")
php_install_packages=""

# Check if the version is valid
if [ -z "$php_version" ]; then
    echo "Please specify a valid PHP version"
    exit 1
fi

echo "The following package will be installed:"

for i in "${php_packages_list[@]}"
do
   :
   #replace [php_ver] with the version of php you want to install
   php_package=${i/\[php_ver\]/$php_version}
   php_install_packages="$php_install_packages $php_package"
   echo $php_package
done

sudo apt-get install -y "${php_install_packages}"