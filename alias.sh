# LAMP Stack: Apache/PHP/MySQL
alias lampstatus="sudo service apache2 status ; sudo service mysql status"
alias lampstart="sudo service mysql start ; sudo service apache2 start"
alias lampstop="sudo service mysql stop ; sudo service apache2 stop"
alias lamprestart="lampstop ; lampstart"

# PHP Helpers alias
source "/home/otls/aliases/php/alias.sh"

# Laravel Helpers alias
source "/home/otls/aliases/laravel/alias.sh"

# Composer
alias composer-update-no-limit="php -d memory_limit=-1  /usr/local/bin/composer update"
alias composer-install-no-limit="php -d memory_limit=-1  /usr/local/bin/composer install"

# shortened aliases
## sudo
alias su-update="sudo apt-get update"
alias su-upgrade="sudo apt-get upgrade"
alias su-dist-upgrade="sudo apt-get dist-upgrade"
alias su-autoremove="sudo apt-get autoremove"
alias su-clean="sudo apt-get clean"
alias su-autoclean="sudo apt-get autoclean"
alias su-check="sudo apt-get check"
alias su-show-updates="sudo apt-get upgrade -s"
alias su-install="sudo apt-get install"

alias bash-refresh="source ~/.bashrc && echo 'Bash refreshed'"